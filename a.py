import network
import urequests
import time

import machine
import onewire
import ds18x20
import time

import config

# Initialize the GPIO pin for one-wire data
dat_pin = machine.Pin(4)  # Replace 4 with the GPIO pin you're using

# Create the onewire and ds18x20 objects
ds_sensor = ds18x20.DS18X20(onewire.OneWire(dat_pin))

# Scan for sensors, assuming only one is connected
roms = ds_sensor.scan()
print('Found DS devices:', roms)


# Wi-Fi credentials
ssid = config.ssid
password = config.password

# Connect to Wi-Fi
station = network.WLAN(network.STA_IF)
station.active(True)
station.connect(ssid, password)

# Wait for connection
while not station.isconnected():
    time.sleep(1)

print('Connected to Wi-Fi')

# Pushgateway configuration
pushgateway_url = config.pushgateway_url

def send_temperature(temperature):
    data = 'temperature{{sensor="sensor1"}} {}\n'.format(temperature)
    response = urequests.post(pushgateway_url, data=data)
    if response.status_code == 200:
        print("Data sent successfully")
    else:
        print("Failed to send data")
    response.close()

def read_temperature():
    ds_sensor.convert_temp()
    time.sleep_ms(750)  # DS18B20 conversion time (could be up to 750ms)
    temp = ds_sensor.read_temp(roms[0])
    print('Temperature:', temp)
    return temp

while True:
    temperature = read_temperature()
    send_temperature(temperature)
    time.sleep(60)

